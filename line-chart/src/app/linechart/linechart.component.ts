import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css']
})
export class LinechartComponent implements OnInit {

  constructor() { }

  chartOptions = {
    responsive: true
  };


  linechartData = [
    { data: [20,21,31,49,21,50,52,53,54,62,63 ], label: 'SERIES 1' },
    { data: [10,12,16,37,18,24,21,23,31,32,22], label: 'SERIES 2' },
    { data: [62,21,11,29,21,20,42,23,34,63,12], label: 'SERIES 3' },
    { data: [23,11,31,11,60,72,23,44,21,63,21], label: 'SERIES 4' }
  ];

  linechartLabels = ['1990', '1991', '1992', '1993','1994','1995','1996','1997','1998','1999','2000','2001'];

  onChartClick(event) {
    console.log(event);
  }

  ngOnInit() {
  }

}
